/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package src.com.compdev.bo;

import com.compdev.bo.BookAppointment;
import com.compdev.dbutil.TestDB;
import com.compdev.exceptions.DatabaseException;
import com.compdev.exceptions.LAMSException;
import com.compdev.vo.Appointment;
import com.compdev.vo.Patient;
import java.util.HashMap;
import org.hibernate.exception.GenericJDBCException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author student
 */
public class BookAppointmentTest {
    
    Appointment newValidAppt;
    Appointment newInvalidAppt;
    HashMap hm;
    
    public BookAppointmentTest() {
        
        hm = new HashMap();
        hm.put("86900", "292.9");
        
        newValidAppt = new Appointment();
        newValidAppt.setAppointmentNumber("800");
        newValidAppt.setPatientId("210");
        newValidAppt.setPhlebotomistId("100");
        newValidAppt.setPatientServiceCenterCode("500");
        newValidAppt.setPhysicianId("10");
        newValidAppt.setAppointmentDate("2013-10-10");
        newValidAppt.setAppointmentTime("11:00");
        newValidAppt.setTestDetails(hm);
        
        newInvalidAppt = new Appointment();
        newInvalidAppt.setAppointmentNumber("800");
        newInvalidAppt.setPatientId("310");
        newInvalidAppt.setPhlebotomistId("600");
        newInvalidAppt.setPatientServiceCenterCode("700");
        newInvalidAppt.setPhysicianId("80");
        newInvalidAppt.setAppointmentDate("2000-10-10");
        newInvalidAppt.setAppointmentTime("12:00");
        newInvalidAppt.setTestDetails(hm);
    }
    
    @BeforeClass
    public static void setUpClass() {
        TestDB.main(null);
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of checkAvailability method, of class BookAppointment.
     * Invalid appointment
     */
    @Test(expected=LAMSException.class) 
    public void testCheckAvailability_InvalidAppointment() throws Exception {
        
        System.out.println("TESTING...checkAvailability_InvalidAppointment");  
        // Test should be false as database contains existing appointment
        assertFalse(new BookAppointment().checkAvailability(newInvalidAppt));
    }
    
     /**
     * Test of checkAvailability method, of class BookAppointment.
     * Valid appointment; does not exist within database
     */
    @Test
    public void testCheckAvailability_ValidAppointment() throws Exception {        
        System.out.println("TESTING...checkAvailability_ValidAppointment");
        // ensure new time is scheduled if other tests are ran prior to executing this
        newValidAppt.setAppointmentTime("12:00"); 
        // Test should be true as database does not contain appointment
        assertTrue(new BookAppointment().checkAvailability(newValidAppt));
    }
    
    
    /**
     * Test of makeAppointment method, of class BookAppointment.
     */
    @Test
    public void testMakeAppointment() throws Exception {
        // ensure new time is scheduled if other tests are ran prior to executing this
        newValidAppt.setAppointmentTime("13:00"); 
        // Test should be true as database does not contain appointment
        new BookAppointment().makeAppointment(newValidAppt);
    }

    /**
     * Test of getNextAppointment method, of class BookAppointment.
     * Valid new valid appointment
     */
    @Test
    public void testGetNextAppointment() throws Exception {
       new BookAppointment().getNextAppointment(newValidAppt);
    }

    /**
     * Test of getPatientInfoById method, of class BookAppointment.
     */
    @Test
    public void testGetPatientInfoById_ValidInput() throws Exception {
        String patientID = "210";
        new BookAppointment().getPatientInfoById(patientID);
    }
    
    /**
     * Test of getPatientInfoById method, of class BookAppointment.
     * With non-integer characters (invalid) included in patient ID
     */
    @Test (expected=LAMSException.class)
    public void testGetPatientInfoById_InvalidInput() throws Exception {
       String patientID = "ACB123"; //invalid characters
       new BookAppointment().getPatientInfoById(patientID);
    }

    /**
     * Test of getPatientInfoByDetails method, of class BookAppointment
     * with valid first name, last name and DoB
     */
    @Test 
    public void testGetPatientInfoByDetails_ValidInput() throws Exception {
        Patient patient1 = new Patient();
        patient1.setDob("1959-09-22");
        patient1.setName("Tom Thumb");
        
        new BookAppointment().getPatientInfoByDetails(patient1);
        // should pass
    }
    
    /**
     * Test of getPatientInfoByDetails method, of class BookAppointment.
     * with an invalid date
     */
    @Test
    public void testGetPatientInfoByDetails_InvalidInput() throws Exception {
        Patient patient1 = new Patient();
        patient1.setDob("2000-11-30"); // non-matched DoB
        patient1.setName("Mary Sue"); // non-existant Name
        
        new BookAppointment().getPatientInfoByDetails(patient1);
        // should pass even with mismatched DoB and name
    }
    
    /**
     * Test of getPatientInfoByDetails method, of class BookAppointment.
     * with a null name
     */
    @Test (expected=LAMSException.class)
    public void testGetPatientInfoByDetails_NullInput() throws Exception {
        Patient patient1 = new Patient();
        patient1.setDob(null); 
        patient1.setName(null); 
        
        new BookAppointment().getPatientInfoByDetails(patient1);
        // should fail, needs to be documented
    }
}